<?php

/**
 * @module          KeepOut
 * @author          cms-lab
 * @copyright       2023-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


if(isset ($_GET['tool'])) 
{
	$toolname = $_GET['tool'];
} 
else 
{
	die('[1]');
}

// get instance of functions file
$oKO = keepout::getInstance();

if(isset ($_GET['tool']) && (empty($_POST)) )  
{
	$oKO->list_entries();
}

if(isset ($_POST['show_info']) && ($_POST['show_info']== 'show') ) 
{
	$oKO->show_info();
}

if(isset ($_POST['add_entry']) ) 
{
	$oKO->edit_entry('-1');
}

if(isset ($_POST['edit_entry']) ) 
{
	$oKO->edit_entry($_POST['edit_entry']);
}

if(isset ($_POST['save_entry']) ) 
{
	$oKO->save_entry($_POST['save_entry']);
}

if(isset ($_POST['delete_entry']) ) 
{
	$oKO->delete_entry($_POST['delete_entry']);
}

//$admin->print_footer();
