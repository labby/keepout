<?php

/**
 * @module          KeepOut
 * @author          cms-lab
 * @copyright       2023-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$MOD_KEEPOUT = [
	'action'	    => "Action",
	'add'	    	=> "Add entry",
	'all_entries'  	=> "All entries",
	'delete'     	=> "delete",
	'delete_ok'     => "Entry delete successful",
	'edit'	        => "Edit",
	'email'	        => "Email",	
	'error'	        => "Error",
	'error_double'	=> "An entry is already existing!",
	'info'	        => "Addon Info",
// table header	
	'header0'	    => "IP-Address",
	'header1'	    => "ID",
	'header2'	    => "Referrer",
	'header3'	    => "Created",
	
	'help'	    	=> "Help",
	'save_ok'	    => "Data save successful",
	'to_delete'	    => " ",
	'want_delete'	=> "Do you want to delete"
];
