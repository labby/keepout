<?php

/**
 * @module          KeepOut
 * @author          cms-lab
 * @copyright       2023-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$MOD_KEEPOUT = [
	'action'	    => "Aktion",
	'add'	    	=> "Eintrag hinzufügen",
	'all_entries'  	=> "Alle Einträge",
	'delete'     	=> "Löschen",
	'delete_ok'     => "Datensatz erfolgreich gelöscht",
	'edit'	        => "Bearbeiten",
	'email'	        => "Email",	
	'error'	        => "FEHLER",
	'error_double'	=> "Es ist bereits ein Datensatz vorhanden!",	
	'info'	        => "Addon Info",
// table header	
	'header0'	    => "IP-Adresse",
	'header1'	    => "ID",
	'header2'	    => "Referrer",
	'header3'	    => "Erstellt",
	
	'help'	    	=> "Hilfe-Seite",
	'save_ok'	    => "Daten erfolgreich gespeichert",
	'to_delete'	    => "wirklich löschen",
	'want_delete'	=> "Wollen Sie den Datensatz"
];
