<?php

/**
 * @module          KeepOut
 * @author          cms-lab
 * @copyright       2023-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$module_directory	= 'keepout';
$module_name		= 'KeepOut';
$module_function	= 'tool';
$module_version		= '1.2.0';
$module_platform	= '7.x';
$module_delete		=  true;
$module_author		= 'cms-lab';
$module_home		= 'https://cms-lab.com';
$module_guid		= 'a4e20ac3-4cc6-4757-9529-473071fc1a1f';
$module_license		= '<a href="https://cms-lab.com/_documentation/keepout/license.php" target="_blank">Custom License</a>';
$module_license_terms	= '<a href="https://cms-lab.com/_documentation/keepout/license.php" target="_blank">see License</a>';
$module_description	= 'Admintool GUI to prevent registering and send forms by blocking mailadresses and/or ipadresses.';
