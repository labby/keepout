### KeepOut
=========

LEPTON Admintool GUI to prevent registering and send forms by blocking mailadresses and/or ipadresses.


#### Requirements

* [LEPTON CMS][1], Version see precheck.php
 

#### Installation

* download latest [KeepOut.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon go to admintools, enter KeepOut and start to work. <br />
For further details please see [readme file][3].


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: https://lepton-cms.com/lepador/admintools/keepout.php
[3]: https://cms-lab.com/_documentation/keepout/readme.php
