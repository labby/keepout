<?php

/**
 * @module          KeepOut
 * @author          cms-lab
 * @copyright       2023-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
class keepout extends LEPTON_abstract
{
	public array $all_entries = [];	
	public string $addon_color = 'blue';
	public string $action_url = ADMIN_URL . '/admintools/tool.php?tool=keepout';
	public string $action = LEPTON_URL . '/modules/keepout/';

	public object|null $oTwig = null;
	public LEPTON_admin $admin;	
	public LEPTON_database $database;
	static $instance;	

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();		
		$this->admin = LEPTON_admin::getInstance('Pages','Start',false,false);
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('keepout');		
		$this->init_tool();		
	}
	
	public function init_tool( $sToolname = '' )
	{
		// Get all keepouts
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."keepout ORDER BY id DESC",
			true,
			$this->all_entries,
			true
		);
	}

	public function list_entries()
	{
		// data for twig template engine	
		$data = [
			'oKO'			=> $this,	
			'readme_link'	=> "https://cms-lab.com/_documentation/keepout/readme.php",			
			'leptoken'		=> get_leptoken()
			];
 
		// get the template-engine.	
		echo $this->oTwig->render( 
			"@keepout/list.lte",	//	template-filename
			$data					//	template-data
		);
	
	}

	public function edit_entry($id) 
	{
        if ((is_numeric($id)) && ($id == -1))
		{
			$last_id = $this->database->get_one("SELECT MAX(id) FROM ".TABLE_PREFIX."keepout");
			$fields = [
				'id' => $last_id + 1 ,
				'email' => 'no@none.tld'	
			];
		
			$result = $this->database->build_and_execute (
				"INSERT",
				TABLE_PREFIX."keepout",
				$fields
			);

			$this->admin->print_success($this->language['save_ok'], ADMIN_URL.'/admintools/tool.php?tool=keepout');
		}		
		elseif ((is_numeric($id)) && ($id > 0))
		{
			$entry = intval($id);
			$current_entry = [];
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."keepout WHERE id = ".$entry ,
				true,
				$current_entry,
				false
			);		
				
			// data for twig template engine	
			$data = [
				'oKO'			=> $this,
				'current_entry'=> $current_entry,
				'leptoken'		=> get_leptoken()
			];

			// get the template-engine		
			echo $this->oTwig->render( 
				"@keepout/edit.lte",	//	template-filename
				$data					//	template-data
			);				
		}
	}
	
	public function save_entry($id) 
	{
		if (is_numeric($id)  && $id > 0 )			
		{
			$entry_id = intval($id);

			// check for doubles
			$double_ip = $this->database->get_one("SELECT ip FROM ".TABLE_PREFIX."keepout WHERE id = ".$entry_id);
			$double_ip = ($double_ip == '' ? '0' : $_POST['ip']);
			
			$double_email = $this->database->get_one("SELECT email FROM ".TABLE_PREFIX."keepout WHERE id = ".$entry_id);
			$double_email = ($double_email == 'no@none.tld' ? '0' : $_POST['email']);
			
			$double_referrer = $this->database->get_one("SELECT referrer FROM ".TABLE_PREFIX."keepout WHERE id = ".$entry_id);
			$double_referrer = ($double_referrer == '' ? '0' : $_POST['referrer']);
	
			if($double_ip == $_POST['ip'] || $double_email == $_POST['email'] || $double_referrer == $_POST['referrer'] )
			{
				$this->admin->print_error($this->language['error_double'], ADMIN_URL.'/admintools/tool.php?tool=keepout');	
			}
			
			$request = LEPTON_request::getInstance();
			$all_names = [
				'ip'		=> [ 'type' => 'string_clean', 'default' => "" ],
				'email'		=> [ 'type' => 'string_clean', 'default' => "" ],
				'referrer'	=> [ 'type' => 'string_clean', 'default' => "" ],
				'active'	=> [ 'type' => 'integer', 'default' => "1" ]
			];		

			$all_values = $request->testPostValues($all_names);			
			$table = TABLE_PREFIX."keepout";
			$this->database->build_and_execute( 'UPDATE', $table, $all_values,'id = '.$entry_id);
			
			$this->admin->print_success($this->language['save_ok'], ADMIN_URL.'/admintools/tool.php?tool=keepout');			
		}		
	}

	
	public function delete_entry($id) 
	{
		if (is_numeric($id)  && $id > 0 )			
		{
			$to_delete = intval($id);
			
			$this->database->simple_query("DELETE FROM ".TABLE_PREFIX."keepout WHERE id = ".$to_delete);					
			$this->admin->print_success($this->language['delete_ok'], ADMIN_URL.'/admintools/tool.php?tool=keepout');			
		}		
	}	
	


	public function show_info() 
	{
		// create links	
		$support_link = "<a href='#'>NO Live-Support / FAQ</a>";
		$readme_link = "<a href='https://cms-lab.com/_documentation/keepout/readme.php' class='info' target='_blank'>Readme</a>";		

		// data for twig template engine	
		$data = array(
			'oKO'			=> $this,
			'readme_link'	=> $readme_link,		
			'SUPPORT'		=> $support_link,		
			'image_url'		=> 'https://cms-lab.com/_documentation/media/keepout/keepout.jpg'
			);

			// get the template-engine		
			echo $this->oTwig->render(
			"@keepout/info.lte",	//	template-filename
			$data					//	template-data
		);	
	}		
}